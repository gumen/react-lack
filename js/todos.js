import database from './database.js'

export const get = () => {
  return database.get('todos') || []
}

export const add = (description) => {
  const todos = get()
  const newTodo = {
    id          : todos.reduce((max, {id}) => Math.max(id, max), 0) + 1,
    completed   : false,
    description : description,
  }
  database.set('todos', [...todos, newTodo])
  return newTodo
}

export const update = (id, data) => {
  database.set('todos', get().map(todo => {
    if (todo.id === id) {
      if (data.completed   !== undefined) todo.completed   = data.completed
      if (data.description !== undefined) todo.description = data.description
    }
    return todo
  }))
}

export const remove = (id) => {
  database.set('todos', get().filter(todo => todo.id !== id))
}

export default { get, add, update, remove }
