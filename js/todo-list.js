import { get as getTodos } from './todos.js'
import './todo-add.js'
import './todo-item.js'

class TodoList extends HTMLElement {
  static createTodoItem(data) {
    const elementTodoItem = document.createElement('todo-item')

    elementTodoItem.dataset.id          = data.id
    elementTodoItem.dataset.completed   = data.completed
    elementTodoItem.dataset.description = data.description

    return elementTodoItem
  }

  constructor() {
    super()
    const shadowRoot = this.attachShadow({mode: 'open'})
    const todosList = getTodos()
    const elementTodoAdd = document.createElement('todo-add')

    shadowRoot.appendChild(elementTodoAdd)

    todosList.forEach(todo => {
      shadowRoot.appendChild(TodoList.createTodoItem(todo))
    })

    elementTodoAdd.addEventListener('add', event => {
      shadowRoot.appendChild(TodoList.createTodoItem({
        id          : event.detail.id,
        completed   : event.detail.completed,
        description : event.detail.description,
      }))
    })
  }
}

customElements.define('todo-list', TodoList)
