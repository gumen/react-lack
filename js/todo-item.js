import todos from './todos.js'

class TodoItem extends HTMLElement {
  static get observedAttributes() {
    return [
      'data-id',
      'data-completed',
      'data-description',
    ]
  }

  constructor() {
    super()

    const shadowRoot = this.attachShadow({mode: 'open'})

    shadowRoot.innerHTML = `
      <link href="css/todo-item.css" rel="stylesheet">
      <input type="checkbox">
      <label></label>
      <button>remove</button>
    `

    this.elements = {}

    this.elements.input = shadowRoot.querySelector('input')
    this.elements.label = shadowRoot.querySelector('label')

    this.elements.input.checked = this.dataset.completed === 'true'

    // checkbox - toggle "completed"
    this.elements.input.addEventListener('change', () => {
      this.dataset.completed = this.elements.input.checked

      todos.update(+this.dataset.id, {
        completed: this.elements.input.checked
      })
    })

    // button - remove item on click
    shadowRoot.querySelector('button').addEventListener('click', () => {
      todos.remove(+this.dataset.id)
      this.remove()
    })
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch(name) {
    case 'data-id':
      this.elements.input.id = newValue
      this.elements.label.setAttribute('for', newValue)
      break;
    case 'data-completed':
      this.elements.input.checked = this.dataset.completed === 'true'
      break;
    case 'data-description':
      this.elements.label.textContent = newValue
      break;
    }
  }
}

customElements.define('todo-item', TodoItem)

