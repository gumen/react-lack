// Fake database

let data = JSON.parse(localStorage.getItem('database') || '{}')

export const set = (key, value) => {
  data[key] = value
  localStorage.setItem('database', JSON.stringify(data))
}

export const get = (key) => key ? data[key] : data

export default { set, get }
