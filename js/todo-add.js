import { add as addTodo } from './todos.js'

class TodoAdd extends HTMLElement {
  constructor() {
    super()

    const shadowRoot = this.attachShadow({mode: 'open'})

    shadowRoot.innerHTML = `
      <link href="css/todo-add.css" rel="stylesheet">
      <form>
        <input type="text" placeholder="Add Todo...">
        <input type="submit" value="Add">
      </form>
    `

    const elementInputText = shadowRoot.querySelector('[type="text"]')

    shadowRoot.querySelector('form').addEventListener('submit', event => {
      event.preventDefault()
      const newTodo = addTodo(elementInputText.value)
      this.dispatchEvent(new CustomEvent('add', { detail: newTodo }))
      elementInputText.value = ''
    })
  }
}

customElements.define('todo-add', TodoAdd)
